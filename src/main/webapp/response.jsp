<%-- 
    Document   : response
    Created on : 23-Oct-2019, 11:34:06
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP-Calc</title>
    </head>
    <body>
        <jsp:useBean id="calc" scope="session" class="com.simonwoodworth.mavenwebapp.Calculator" />
        <h1>Result</h1>
        <p>The answer is <%=calc.addThree(Double.parseDouble(request.getParameter("num1")),
                                            Double.parseDouble(request.getParameter("num2")),
                                            Double.parseDouble(request.getParameter("num3")))%></p>
            
            
    </body>
</html>
